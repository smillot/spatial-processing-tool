# Spatial Processing Tool

## Description
This project is a small demo application written in python to let the user apply image processing on images. I called it Spatial Processing tool because I'll test the application on images coming from the NASA project [New Horizons](https://www.nasa.gov/mission_pages/newhorizons/main/index.html). The images are coming from LORRI instrument and are available from [http://pluto.jhuapl.edu/soc/Pluto-Encounter](http://pluto.jhuapl.edu/soc/Pluto-Encounter/index.php)  
Currently, the application is not intended for usage but only for demo purpose.

## Author
Sebastien Millot

## License

The MIT License (MIT)

Copyright 2023 Sebastien Millot

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Copyrights

All images included in this project are credited to NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute
