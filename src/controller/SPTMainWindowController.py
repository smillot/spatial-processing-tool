# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtCore import QObject, QSize, Qt
from PyQt6.QtWidgets import QFileDialog

from controller.ImageDataController import ImageDataController
from controller.ImageViewerController import ImageViewerController
from controller.ToolboxWidgetController import ToolboxWidgetController

from view.ApplicationMessageWidget import ApplicationMessageWidget
from view.SPTMainWindow import SPTMainWindow
from view.InitialCentralWidget import InitialCentralWidget
from view.ImageViewerCentralWidget import ImageViewerCentralWidget
from view.DockWidget import DockWidget

class SPTMainWindowController(QObject):
    def __init__(self):
        super().__init__()
        # Add main window
        self.main_window = SPTMainWindow()
        self.main_window.resize(QSize(1024, 1024))
        
        # Add central widget
        self.image_viewer_central_widget = ImageViewerCentralWidget()
        self.image_viewer_controller = ImageViewerController(self.image_viewer_central_widget)
        self.main_window.setCentralWidget(self.image_viewer_central_widget)

        # Add the status bar
        self.application_message_widget = ApplicationMessageWidget()
        self.main_window.setStatusBar(self.application_message_widget)
        self.application_message_widget.set_message("Application Ready")

        # Add Dock Widget
        self.dock_widget = DockWidget(self.main_window)
        self.main_window.addDockWidget(Qt.DockWidgetArea.RightDockWidgetArea, self.dock_widget)
        self.toolbox_controller = ToolboxWidgetController()
        self.toolbox_controller.createToolboxWidget(self.dock_widget)
        self.dock_widget.setWidget(self.toolbox_controller.toolbox_widget)
        self.toolbox_controller.toolbox_widget.m_pushButtonOpenData.clicked.connect(self.on_open_data_clicked)

        # Initialize internal controllers
        self.image_data_controller = ImageDataController()
        self.image_data_controller.image_read.connect(self.on_image_read)

    def start(self):
        self.main_window.show()

    def on_open_data_clicked(self):
        image_path = QFileDialog.getOpenFileName(None, "Open Image", "img", ("Image Files (*.png, *.jpg)"))
        if len(image_path) == 0 or image_path[0] == '':
            return
        
        self.image_data_controller.read_image(image_path[0])

    def on_image_read(self, image_path):
        self.application_message_widget.set_image_loaded_message(image_path)
        self.image_viewer_controller.render_loaded_image(self.image_data_controller.image_storage.loaded_image)
        
