# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtCore import QObject
from view.ToolboxWidget import ToolboxWidget

class ToolboxWidgetController(QObject):
    def __init__(self):
        super().__init__()
        self.toolbox_widget = None


    def createToolboxWidget(self, parent=None):
        self.toolbox_widget = ToolboxWidget(parent)
