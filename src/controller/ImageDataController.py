# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtCore import QObject, pyqtSignal

from IO.ImageReader import ImageReader

from model.ImageStorage import ImageStorage

class ImageDataController(QObject):

    image_read = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        # Initialize the model that will contains all the images
        self.image_storage = ImageStorage()
    
    def read_image(self, image_path):
        # Read image
        image_reader = ImageReader()
        image_data = image_reader.read(image_path)

        # Store image
        self.image_storage.set_loaded_image(image_data, image_path)

        self.image_read.emit(image_path)
