# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtCore import QObject

class ImageViewerController(QObject):
    def __init__(self, image_viewer_widget=None):
        super().__init__()
        self.image_viewer_widget = image_viewer_widget

    def render_loaded_image(self, image_data):
        self.image_viewer_widget.render_image(image_data)
