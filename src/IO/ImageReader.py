# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from skimage import io

class ImageReader():
    def __init__(self):
        self.image_path = None
        self.image_data = None
    
    def read(self, image_path):
        self.image_path = image_path

        try:
            self.image_data = io.imread(self.image_path)

        except:
            self.image_path = None
            self.image_data = None
            print("Error while loading data {0}".format(image_path))
        
        return self.image_data
