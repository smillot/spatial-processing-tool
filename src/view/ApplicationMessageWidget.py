# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtWidgets import QStatusBar, QVBoxLayout, QLabel

class ApplicationMessageWidget(QStatusBar):
    def __init__(self, parent=None):
        super().__init__()

        self.message_label = QLabel(parent=self)
        self.addPermanentWidget(self.message_label)

    def set_image_loaded_message(self, image_path):
        self.message_label.setText("Image Loaded: {0}".format(image_path))

    def set_message(self, msg):
        self.message_label.setText(msg)
