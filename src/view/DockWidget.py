# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtWidgets import QDockWidget
from PyQt6.QtCore import Qt

class DockWidget(QDockWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.setWindowTitle("Toolbox")
        self.setFeatures(QDockWidget.DockWidgetFeature.DockWidgetMovable)
