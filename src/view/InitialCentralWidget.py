# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtWidgets import QWidget, QPushButton, QVBoxLayout

class InitialCentralWidget(QWidget):
    def __init__(self):
        super().__init__()

        layout = QVBoxLayout()
        self.open_data_button = QPushButton("Open Data")
        layout.addWidget(self.open_data_button)
        self.setLayout(layout)
