# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6.QtWidgets import QMainWindow

class SPTMainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Spatial Processing Tool")