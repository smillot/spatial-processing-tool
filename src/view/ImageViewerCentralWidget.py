# -*- coding: utf-8 -*-
"""
author: Sebastien Millot
"""

from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from matplotlib.figure import Figure

class ImageViewerCentralWidget(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.sub_plot = fig.add_subplot()
        super(ImageViewerCentralWidget, self).__init__(fig)

    def render_image(self, image_data):
        self.sub_plot.clear()
        self.sub_plot.imshow(image_data, cmap='Greys_r')
        self.draw()
