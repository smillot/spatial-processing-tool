# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from PyQt6 import uic
from PyQt6.QtWidgets import QWidget

class ToolboxWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi("src/view/ToolboxWidget.ui", self)
