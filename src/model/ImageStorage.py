# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

class ImageStorage():
    def __init__(self):
        self.loaded_image = None
        self.loaded_image_filename = None

    def set_loaded_image(self, loaded_image, loaded_image_path):
        self.loaded_image = loaded_image
        self.loaded_image_filename = loaded_image_path
