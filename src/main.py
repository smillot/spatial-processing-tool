# -*- coding: utf-8 -*-
"""
author: Sebastien Millot
"""

from PyQt6.QtWidgets import QApplication
import sys
from controller.SPTMainWindowController import SPTMainWindowController

app = QApplication(sys.argv)

main_controller = SPTMainWindowController()
main_controller.start()

app.exec()
