# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

from filters.FilterBase import FilterBase

class GaussianFilter(FilterBase):
    def __init__(self):
        super().__init__()
        self.kernel_size = 3
    
    def set_kernel_size(self, kernel_size):
        self.kernel_size = kernel_size

    

