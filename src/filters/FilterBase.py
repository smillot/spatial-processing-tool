# -*- coding: utf-8 -*-

"""
author: Sebastien Millot
"""

class FilterBase():
    def __init__(self):
        self.input_image = None
        self.result_image = None

    def compute(self):
        pass

    def set_input_image(self, input_image):
        self.input_image = input_image
    
    def get_result_image(self):
        return self.result_image
